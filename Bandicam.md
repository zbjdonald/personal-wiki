# Bandicam

Bandicam 是一款韩国产的录屏软件。

快捷键：

- F12 开始/关闭录制
- Shift + F12 暂停录制
- ctrl + alt + D 进入编辑模式

#### 1. [屏幕录制方式](https://www.bandicam.cn/how-to/)

点击左侧 Home，选取需要的录制模式：支持选区录制，全屏，游戏模式等录制，具体见文档。

#### 2. [常规设置](https://www.bandicam.cn/support/settings/general/)

选择视频输出位置，是否开启写缓存等。

### 3. [视频设置](https://www.bandicam.cn/support/settings/video/)

设置视频格式，帧率等参数，根据自身硬件的不同选择不同的编码方式。

[编码器区别（视频/音频）](https://www.bandicam.cn/support/tips/best-codec/)

![1552812241547](C:\Users\zbjdo\AppData\Roaming\Typora\typora-user-images\1552812241547.png)

如果有后期剪辑需求，建议使用 [CFR(恒定帧率)](https://www.bandicam.cn/support/tips/vfr-cfr/)，可以防止一些声画不同步的问题。

