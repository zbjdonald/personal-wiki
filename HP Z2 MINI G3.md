# HP z2 mini G3 汇总

[HP Z2 MINI](https://www8.hp.com/sg/en/workstations/z2mini/index.html?jumpid=in_r12139_sg/en/psg/ws_creative_photo/cross-link/z2-mini-learn-more)

[HP Z2 Mini G3 工作站产品规格](https://support.hp.com/cn-zh/document/c05348345)

[HP Z2 Mini G3 驱动](https://support.hp.com/ie-en/drivers/selfservice/hp-z2-mini-g3-workstation/12716708)

FAQ：

[HP Z2 + Intel 8265 + WiFi/BT Antennen 网卡天线安装讨论](https://h30434.www3.hp.com/t5/Business-PCs-Workstations-and-Point-of-Sale-Systems/HP-Z2-Intel-8265-WiFi-BT-Antennen/td-p/6201262)

#### 支持的CPU列表

![1553939769127](/home/zbj/.config/Typora/typora-user-images/1553939769127.png)

[HP Z2 无线网卡 安装](http://www.supportvideos.ext.hp.com/detail/video/5819967000001/how-to-remove-and-replace-the-wireless-lan-module-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 无线网卡天线 安装](http://supportvideos.ext.hp.com/detail/video/5819966999001/how-to-replace-the-wireless-antennas-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 CPU 安装](http://supportvideos.ext.hp.com/detail/video/5819967698001/how-to-replace-the-cpu-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 内存安装](http://supportvideos.ext.hp.com/detail/video/5819974209001/how-to-remove-and-replace-the-memory-modules-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 M2 ssd 安装](http://supportvideos.ext.hp.com/detail/video/5819967700001/how-to-remove-and-replace-the-m.2-solid-state-drive-assembly-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 2.5寸机械盘安装](http://www.supportvideos.ext.hp.com/detail/video/5819969394001/how-to-remove-and-replace-the-2.5in-storage-drive-assembly-for-the-hp-z2-mini-g3-and-g4-workstations)

[HP Z2 显卡安装](http://supportvideos.ext.hp.com/detail/video/5819965290001/how-to-remove-and-replace-the-mxm-graphics-card-for-the-hp-z2-mini-g3-and-g4-workstations)

[显示器支架安装](https://site-853123.bcvp0rtal.com/detail/video/4715538811001/hp-elitedisplay-directly-attached-to-an-hp-desktop-mini?autoStart=true&q=mini)



[把 Ubuntu 16.04 及 18.04 安裝到幾款特殊的 NVMe SSD 上](https://medium.com/@honglong/%E6%8A%8A-ubuntu-16-04-%E5%8F%8A-18-04-%E5%AE%89%E8%A3%9D%E5%88%B0%E5%B9%BE%E6%AC%BE%E7%89%B9%E6%AE%8A%E7%9A%84-nvme-ssd-%E4%B8%8A-504a519ab729)

[what is the correct and reliable way to freshly install Ubuntu Gnome in an NVMe SSD with UEFI enabled?](https://askubuntu.com/questions/1011821/what-is-the-correct-and-reliable-way-to-freshly-install-ubuntu-gnome-in-an-nvme)



https://www.maketecheasier.com/sync-onedrive-linux/