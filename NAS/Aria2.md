# Aria2 教程

[在群晖 DSM Docker 安装整套下载管理工具“Aria2 + AriaNg + File Manager”](https://www.jkg.tw/?p=1178)

[Aria2，目前我认为最好的下载工具](http://stonebythesea.org/posts/using-aria2/)

[玩转群晖NAS--下载神器aria2](https://www.itfanr.cc/2017/11/17/playing-synology-nas-of-docker-aria2/)

[使用 aria2 下载百度网盘文件](https://medium.com/@xavieris/%E4%BD%BF%E7%94%A8-aria2-%E4%B8%8B%E8%BD%BD%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98%E6%96%87%E4%BB%B6-459b52e2802c)

[Aria2-不限速全平台下载利器](https://www.jianshu.com/p/bb9490ea1c4d)

[Aria2基础上手指南](https://zhuanlan.zhihu.com/p/30666881)

[Aria2官网](https://aria2.github.io/)

aria2 是一个命令行下载工具，类似于 curl、wget，但是功能更强，亮点是多线程下载、BT 下载。aria 支持的协议/下载源有：http/s、ftp、BitTorrent、Metalink 等等。

> aria2 is a **lightweight** multi-protocol & multi-source command-line **download utility**. It supports **HTTP/HTTPS**, **FTP**, **SFTP**, **BitTorrent** and **Metalink**. aria2 can be manipulated via built-in **JSON-RPC** and **XML-RPC** interfaces.

群晖自带的 Download Station 已经足够好用，可以满足 BT，youtube 等下载需求，但是不能高速下载百度网盘。

Aria2 是命令行工具，有大神做了前端还有配套的一些工具，可以利用 Docker 安装，比较推荐的一款镜像是 [wahyd4/aria2-ui](https://hub.docker.com/r/wahyd4/aria2-ui/)，包含 Aria2、AriaNg 和File Manager，主要方便那些用户期望只运行一个镜像就能实现图形化下载文件和在线播放文件。

具体 set up 教程见视频：TODO 录制设置视频

## [AriaNg](https://github.com/mayswind/AriaNg)

[aria2+AriaNg 打造自己的离线下载/云播平台](https://yorkchou.com/aria2.html)

AriaNg 是一个让 aria2 更容易使用的现代 Web 前端。



还是需要VIP的 算了

https://www.weibo.com/vposy?from=myfollow_all&is_all=1#1552220428922

https://pan.baidu.com/s/1GZmjYejMcXjwPZ4zSUn_cQ#list/path=%2F%E5%9B%BD%E5%A3%AB%E6%97%A0%E5%8F%8C%E6%8A%A2%E6%9A%97%E6%9D%A0%2FAcrobat&parentPath=%2F