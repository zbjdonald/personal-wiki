# SS | SSR 服务端

不推荐自己架设 ss / ssr 服务端，容易被封，时间长了线路性能会不好，建议购买机场的节点，根据不同的网络环境选择合适的节点。



[浅谈部分机场（SS/SSR提供商）的使用感受--持续更新中](https://www.evernote.com/shard/s609/client/snv?noteGuid=087d120a-d07f-4b13-95d4-95af5d573db5&noteKey=43c350e6c55ac4c3&sn=https%3A%2F%2Fwww.evernote.com%2Fshard%2Fs609%2Fsh%2F087d120a-d07f-4b13-95d4-95af5d573db5%2F43c350e6c55ac4c3&title=%25E6%25B5%2585%25E8%25B0%2588%25E9%2583%25A8%25E5%2588%2586%25E6%259C%25BA%25E5%259C%25BA%25EF%25BC%2588SS%252FSSR%25E6%258F%2590%25E4%25BE%259B%25E5%2595%2586%25EF%25BC%2589%25E7%259A%2584%25E4%25BD%25BF%25E7%2594%25A8%25E6%2584%259F%25E5%258F%2597--%25E6%258C%2581%25E7%25BB%25AD%25E6%259B%25B4%25E6%2596%25B0%25E4%25B8%25AD)

https://www.jianshu.com/p/6e551f97555a

小机场：

- MENGDI-SS
  - 节点种类为 ssr v2ray
  - 官网：[MENGDI-SS](https://www.shadowsock5.com/)
  - [telegram官方频道](https://t.me/JUXIANGE)
  - [官方TG群组](https://t.me/MdCloud)

大机场：

- 召唤师
  - 节点种类为 ssr ss v2ray
  - 官网：**https://zhs.ooo**, https://zhs.tw/
  - [telegram 官方频道](https://t.me/lubantips)

