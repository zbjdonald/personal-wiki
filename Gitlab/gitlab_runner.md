## Gitlab Runner

[Runner](https://docs.gitlab.com/ce/ci/runners/README.html) 可以是虚拟机, VPS, 物理机, Docker 容器，甚至是集群。在 [Gitlab Runner 安装](https://docs.gitlab.com/runner/#install-gitlab-runnerinstallindexmd) 中根据需求选择安装方式，例如不同系统下的安装，作为 docker service 如何安装等。安装好后，**根据不同的安装方式** 选择对应的 [Register 方式](https://docs.gitlab.com/runner/register/index.html)。Register 的意思是将安装好的 [Runner](https://docs.gitlab.com/ce/ci/runners/README.html) 同 Gitlab Instance (项目) 绑定。

以 [Runner](https://docs.gitlab.com/ce/ci/runners/README.html) 作为 一个 docker service 为例：

如果按照官方文档，会出现这个问题 [Conflict. The container name “/gitlab-runner” is already in use by container](https://stackoverflow.com/questions/51233747/conflict-the-container-name-gitlab-runner-is-already-in-use-by-container)。方法是直接注册 Runner，并运行:

```bash
# register 
docker run --rm -t -i -v /usr/local/gitlab-ci-runner/config:/etc/gitlab-runner --name gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:3 \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged \
  --locked="false"

# 启动对应runner
docker run -d --name gitlab-runner --restart always \
   -v /usr/local/gitlab-ci-runner/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -v /home/zbjdonald/PycharmProjects/qiaochang:/home/zbjdonald/PycharmProjects/qiaochang \
   gitlab/gitlab-runner:latest \
   --debug run --user=gitlab-runner --working-directory=/home/gitlab-runner
```

> 启动时注意配置文件挂载的位置要与注册时的一致：`-v /usr/local/gitlab-ci-runner/config:/etc/gitlab-runner`

- `PROJECT_REGISTRATION_TOKEN` 存在于 Gitlab 中的 **Settings | CI/CD | Runners settings | Specific Runners | registration token**
- url 为 Gitlab 的地址，例如 `http://192.168.0.24/`

