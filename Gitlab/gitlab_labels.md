# Gitlab lables

### 复制不同项目的 label

@(Copy labels | 复制 labels)

[GitLab Copy](https://github.com/gotsunami/gitlab-copy) 可以在不同 project 之间复制 issues/labels/milestones/notes。

根据系统选择一个 [release](https://github.com/gotsunami/gitlab-copy/releases)，解压后在文件夹里写一个 `gitlab.yml` ，具体见 [Usage](https://github.com/gotsunami/gitlab-copy#usage)。

```yaml
# gitlab.yml
from:
  url: http://192.168.0.24
  token: 点头像 | 左面 user settngs 中的 Account | Private token
  project: memect/MemectExtractor
  labelsOnly: true
to:
  url: http://192.168.0.24
  token: 点头像 | 左面 user settngs 中的 Account | Private token
  project: zbjdonald/tfs
```

- GitLab private tokens: 点头像 | 左面 user settngs 中的 Account | Private token，作用像账号密码。
- url: gitlab 域名
- project: project 页面 url 的非域名部分

```bash
./gitlab-copy gitlab.yml
```

对执行结果没问题后，执行

```bash
./gitlab-copy -y gitlab.yml
```

