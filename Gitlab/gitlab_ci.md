# [Gitlab CI](https://docs.gitlab.com/ce/ci/README.html)

[GitLab-CI 从安装到差点放弃](https://segmentfault.com/a/1190000007180257)

[基于 GitLab 的 CI 实践 docker in docker](https://mp.weixin.qq.com/s?__biz=MzA5OTAyNzQ2OA==&mid=2649698424&idx=1&sn=bb3dedcb7238af5bdb2a54864a5f1dfa&chksm=88930f1bbfe4860d257dbe3570a60bd0a3ed0dde57926f4aa754b4aabe26ed721cfa1f23eea0&mpshare=1&scene=1&srcid=1126r2jisvtTLkaE50FTAiPt&pass_ticket=Wsru6OJd8aNg%2Bbv1kBjfCYVFrtvoipcWn9zY0TM8P5oB61y5nK29u0lTqMorvoP9#rd)

[在docker中执行gitlab-runner](https://segmentfault.com/a/1190000012279248)

Gitlab CI 可以用于测试代码质量，build docker 镜像 push 到私有 registry，部署到生产服务器等。CI/CD 可以有效的规范开发流程，减轻 code review 负担，加快交付速度。

项目托管在gitlab上，在项目中，添加 `.gitlab-ci.yml` ，会开启项目的 CI 部分。当 push 代码时，会触发 CI [pipeline](https://docs.gitlab.com/ce/ci/pipelines.html)，[Gitlab Runner](https://docs.gitlab.com/ce/ci/runners/README.html) 会执行 `.gitlab-ci.yml` 中定义的 pipeline。配置 Gtilab CI 变成两部分：

1. 项目根目录添加 `.gitlab-ci.yml`
2. 安装 | 配置 | 关联 Gitlab Runner

**[Job](https://docs.gitlab.com/ee/ci/yaml/README.html#jobs)** 在 `.gitlab-ci.yml` 中是 top-level elements, 必须最少含有 `script` clause，**release-image** job:

```yaml
release-image:
  stage: deploy
  script:
  - docker build -t "$CONTAINER_RELEASE_IMAGE"
    --build-arg DJANGO_ENV=production -f docker/django/Dockerfile .
  - docker push "$CONTAINER_RELEASE_IMAGE"
  only:
    - master
  environment:
    name: production  # used to track time with 'cycle analytics'
```

**[stage](https://docs.gitlab.com/ee/ci/yaml/README.html#stage)**: 每个 job 属于一个 stage，一个 stage 可以有多个 job, stage 定义在 [stages](https://docs.gitlab.com/ee/ci/yaml/README.html#stages) 中，stages 是 top-level elements，定义了一个 pipeline 中含有多少 stage。属于相同 stage 中的 job 会并行执行。默认 stages 顺序为 build | test | deploy。

```yaml
stages:
  - build
  - test
  - staging
  - production
  
job 1:
  stage: build
  script: make build dependencies
  
job 2:
  stage: staging
  script: make build artifacts
  
job 3:
  stage: test
  script: make test1
  
job 4:
  stage: test
  script: make test2

job 5:
  stage: production
  script: deploy blabla
```

![pipelines](http://wiki-picture.oss-cn-beijing.aliyuncs.com/pipelines.png)

蓝色表示 stage，红色表示 job，**整个图片** 表示 [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html#pipelines)。


## [GitLab CI/CD for Docker](https://docs.gitlab.com/ce/ci/README.html#gitlab-cicd-for-docker)

运行 gitlab runner 相关命令

可以将 job 跑在[`.gitlab-ci.yml`](https://docs.gitlab.com/ce/ci/yaml/README.html) 中定义的 docker 容器里。executor 必须使用 ？？？？

[查看 docker 模式下的 Runner 日志](https://docs.gitlab.com/runner/install/docker.html#reading-gitlab-runner-logs)

如果启动 Runner 时 name 为 gitlab-runner，可以通过 `docker logs gitlab-runner` 查看。如果安装了 [Portainer](https://github.com/portainer/portainer)，可以在 **Containers | gitlab-runner | Container details | Logs** 中查看。

[docker runner 执行 gitlab-runner 命令](https://docs.gitlab.com/runner/install/docker.html#general-gitlab-runner-docker-image-usage)

```bash
docker run [chosen docker options...] gitlab/gitlab-runner [Runner command and options...]
# exmaple, run 'gitlab-runner --help'
docker run --rm -t -i gitlab/gitlab-runner --help

# 对于已经存在的容器,进入容器并执行相关命令
docker exec -it gitlab-runner /bin/bash
## gitlab-runner --help
```

或者在 [Portainer](https://github.com/portainer/portainer) 中的 gitlab-runner 页面上进入 console，然后运行 gitlab-runner 相关命令

```bash
gitlab-runner --help
```

[Gitlab-runner 相关命令](https://docs.gitlab.com/runner/commands/)

```bash
gitlab-runner list # - List all configured runners

```

```
gitlab-runner exec docker build test --docker-privileged 

gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner
```

在 docker 里面 gitlab-runner --debug run



https://gitlab.com/gitlab-org/gitlab-runner/issues/3266

https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#how-docker-integration-works

docker integration 的work



https://forum.gitlab.com/t/how-to-really-debug-a-docker-build-locally/4690

https://bryce.fisher-fleig.org/blog/faster-ci-debugging-with-gitlabci/index.html

https://xrunhprof.wordpress.com/2018/06/26/debugging-gitlab-ci-yml-docker/

https://www.jianshu.com/p/a67270d91fbd



https://gitlab.com/gitlab-org/gitlab-runner/issues/1943

pull policy

```
Executing build stage                               build_stage=upload_artifacts_on_failure job=14851 project=318 runner=d87fcd19
Looking for prebuilt image gitlab/gitlab-runner-helper:x86_64-cf91d5e1 ...  job=14851 project=318 runner=d87fcd19
No credentials found for docker.io                  job=14851 project=318 runner=d87fcd19
```

https://gitlab.hzdr.de/fwcc/gitlab-runner/commit/887091c5cf2916a2537b561fb2a6230c1aa7f7a0

对应 go 代码位置

BuildStageUploadOnFailureArtifacts

https://gitlab.com/gitlab-org/gitlab-runner/issues/114

重要debug 方法



https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/4201

artifacts

debug



做一个整理依赖的镜像，当修改了依赖的时候，重新打包上传，这样能最快速度的 cache dependency

或者 cache 一个通用的镜像，压缩每次构建时间。涉及问题有：

1. 私有 registry 登录问题
2. 基础镜像的手动 dockerfile 修改，如何用两套 poetry.lock，还是维护
3. 维护一个tag，这个tag用干净的镜像构建，测试好后 merge poetry.lock，然后打包新的环境到 docker 上，每次 docker 同步会修改这个镜像？？

```
docker build -t hub.wenyinhulian.cn/qiaochang/qiaochang_base:latest -f /home/zbjdonald/PycharmProjects/qiaochang/docker/django/BASE_Dockerfile . 

docker push hub.wenyinhulian.cn/qiaochang/qiaochang_base:latest

docker tag f22e65c54f05 hub.wenyinhulian.cn/qiaochang/qiaochang:latest
```

docker cache from



deploy 的 script 可以参考一下

```
deploy:
  image: "registry.docker-cn.com/library/centos"
  stage: deploy
  tags:
    - deploy
  script:
    # install ssh client
    - 'ssh-agent || (yum install -y openssh-clients)'
    # run ssh-agent
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    # create ssh dir
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # use ssh-keyscan to get key
    - ssh-keyscan -p $SSH_PORT $DEPLOY_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

    # - ssh -p $SSH_PORT $DEPLOY_USER@$DEPLOY_HOST ls
    - rm -rf .git
    - scp -r -P $SSH_PORT . $DEPLOY_USER@$DEPLOY_HOST:~/we/
```



@(gitlab badge | gitlab 徽章)

[一个各种可用 badge 的网站](https://shields.io/#/)

一个项目有 badge 会显得很高级，同时也能提供一些项目的 meta 信息。Gitlab 默认提供了 coverage 和 pipeline 状态的 badge，设置位置在 **Settings | CI/CD | General pipelines settings** 中的  **Pipeline status** 和 **Coverage report**。

![Pipline status](http://wiki-picture.oss-cn-beijing.aliyuncs.com/badge_demo.png)

蓝色区域用于切换成不同分支的链接，用于更新不同的分支上。


























