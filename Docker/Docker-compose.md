

Docker Compose

[docker-compose 安装](https://github.com/docker/compose/releases)

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

```bash
docker-compose -f docker-compose.yml -f docker/docker-compose.prod.yml config > docker-compose.deploy.yml
```

根据不同的配置文件生成新的配置文件

[Use Compose in production](https://docs.docker.com/compose/production/)

https://runnable.com/docker/advanced-docker-compose-configuration



https://docs.docker.com/compose/extends/#understanding-multiple-compose-files

多文件合成



https://vsupalov.com/difference-docker-compose-and-docker-stack/



docker-compose 和 docker-stack 的区别联系



docker 部署

https://morph027.gitlab.io/post/gitlab-ci-with-docker-swarm/

https://medium.com/@Empanado/simple-continuous-deployment-with-docker-compose-docker-machine-and-gitlab-ci-9047765322e1

https://medium.com/@codingfriend/continuous-integration-and-deployment-with-gitlab-docker-compose-and-digitalocean-6bd6196b502a

这条重点考察一下



# [How to update docker stack without restarting all services](https://stackoverflow.com/questions/44052004/how-to-update-docker-stack-without-restarting-all-services)

docker-compose build --build-arg SOME_ENV=hello web



添加 env



volumes 具体之间调用关系如何整理 比如不同的服务 如何公用 volumes



https://github.com/docker-library/redis/issues/111#issuecomment-349152928

redis docker compose 配置



expose: Exposing ports is unnecessary - services on the same network can access each other's containers on any port.



Opening port 8080 for health checking



https://nickjanetakis.com/blog/docker-tip-28-named-volumes-vs-path-based-volumes

docker-compose volumes