# Portainer

[Portainer documentation](https://portainer.readthedocs.io/en/stable/#portainer-documentation)

[Portainer](https://github.com/portainer/portainer) 是一个轻量级的 docker 环境管理工具，例如可以监控 container 的状态等等，方便部署后管理 docker 环境。

### [部署 Portainer](https://portainer.readthedocs.io/en/latest/deployment.html#deployment)

```bash
docker volume create portainer_data
docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data -v /etc/localtime:/etc/localtime portainer/portainer:linux-amd64-1.20.2
```

> 简单部署方式，适用于大部分场景。

https://github.com/portainer/portainer/issues/913

时区



https://ruiming.me/swarm-mode-particle/

还不错



No administrator account found inside the database (err=Object not found inside the database) (code=404)

https://github.com/portainer/portainer/issues/2332



[Portainer 的 Caddy 配置](https://github.com/portainer/portainer-docs/pull/38#issuecomment-405578507)

```nginx
your.domain {
  # Add the slash on the end, maybe someone knows how to do this in the proxy directive
  redir /portainer /portainer/
  proxy /portainer/ portainer-host:9000 {
    without /portainer
    # Add Forwarded-For headers
    transparent
    # Expect websocket upgrade
    websocket
  }
}
```

### 找回密码

Portainer 现阶段没有找回密码，未来可能加上，详见 [issue](https://github.com/portainer/portainer/issues/1840#issuecomment-384272320)，现阶段根据部署方式，删除 volume, network 等后重新部署。

```bash
# docker stack 方式部署删除
docker stack ls # 看看有没有 portainer
docker stack rm portainer

# docker 方式部署删除
docker ps -a | grep portainer
docker stop 0fd99ee0cb61
docker rm -f 0fd99ee0cb61
docker volume ls | grep portainer # 看看有没有相关的，删掉
docker volume rm portainer_data
```

### FAQ

host 机重装 portainer 出现 403 Forbidden（invalid credentials）

[Unable to reconnect to a swarm after fresh portainer reinstall](https://github.com/portainer/portainer/issues/2133)

[Cant login when mounting the /data volume](https://github.com/portainer/portainer/issues/517)

