# Docker

[如何 10 步 Docker 化一个应用](https://www.hi-linux.com/posts/57498.html)

## [Docker 安装](https://yeasy.gitbooks.io/docker_practice/content/install/ubuntu.html)

安装和开机启动docker
```bash
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh --mirror Aliyun
sudo systemctl enable docker
sudo systemctl start docker
```

需要使用 `docker` 的用户加入 `docker` 用户组
```bash
# 第一次需要建立 docker 用户组
sudo groupadd docker
sudo usermod -aG docker $USER
groups $USER
```

## 基础概念

[Docker Tip #53: Difference between a Registry, Repository and Image](https://nickjanetakis.com/blog/docker-tip-53-difference-between-a-registry-repository-and-image)

[Docker registry vs. repository](https://stackoverflow.com/questions/34004076/difference-between-docker-registry-and-repository)

[Docker Images vs. Containers](https://stackoverflow.com/a/26960888)

**Docker image:** An image is an inert, immutable, file, a snapshot of a container.

**Docker container**: if an image is a class, then a container is an instance of a class—a runtime object.

**Docker repository:** a *collection* of different docker images with same name, that have different tags (同名 image 不同 tag 的集合). e.g. [Python Official Repository](https://hub.docker.com/r/library/python/tags/)

**Docker registry**: a *service* that is storing your docker images.

> Registry 可以有多个 Repositories，Repositories 含有多个通过不同 tag 定义的不同版本的 images.

官方的 docker registry 为: [Docker-hub](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html)，也可以搭建自己的 registry，例如 [harbor](https://github.com/goharbor/harbor)。

Docker Docker image sample: `Registryhost: Port/namespace/repo-name:tag`, 这里的 `namespace` 可以为 group-name，user-name，company-name 等等。[官方文档](https://docs.docker.com/docker-hub/repos/#pushing-a-repository-image-to-docker-hub)这里表述不是很清楚，只是标记为 `<hub-user>`。[wemake.services](https://github.com/wemake-services/wemake-django-template) 的最佳实践是`$REGISTRY/$GROUP_NAME/$PROJECT_NAME: $tag`.

## 常用命令

https://vsupalov.com/docker-arg-env-variable-guide/

## 本地化优化

### 替换 docker hub 源

部署的时候可能会依赖 docker hub，国内网络环境.....，解决方案是搭建自用 registry，或者[替换国内 docker hub 源](https://yeasy.gitbooks.io/docker_practice/install/mirror.html)，例如 ubuntu 16.04+，编辑 `/etc/docker/daemon.json`

```json
{
  "registry-mirrors": [
    "https://registry.docker-cn.com"
  ]
}
```

### 替换 Alpine 源

有一些 DockerFile 会基于 [Alpline 镜像](https://yeasy.gitbooks.io/docker_practice/cases/os/alpine.html)，使用国外镜像速度非常慢，这时可以替换成[中科大 USTC Alpine 源教程](https://mirrors.ustc.edu.cn/help/alpine.html)，在脚本中加入，`sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories`,

```dockerfile
# System deps, add ustc mirror to speed up:
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories \
  && apk --no-cache add \
       bash \
       build-base \
       curl \
       gcc \
       gettext \
       git \
       libffi-dev \
       linux-headers \
       musl-dev \
       postgresql-dev \
       tini \
  && curl -sSL "https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py" | python
```

### 修改镜像时区

[修改 alpine 镜像时区](https://github.com/gliderlabs/docker-alpine/issues/136#issuecomment-341172222)，在 `DockerFile` 中

```
FROM alpine:3.6
RUN apk add --no-cache tzdata
ENV TZ Asia/Shanghai
```

如果想节约镜像体积可以使用 [Alipine: Setting the timezone](https://wiki.alpinelinux.org/wiki/Setting_the_timezone)，设置完时区后删除 tzdata。验证方法：

> 在镜像命令行中输入 date，看是否是 CST 时间。例如 `Tue Jan  8 16:41:09 CST 2019`

修改 ubuntu 镜像时区。