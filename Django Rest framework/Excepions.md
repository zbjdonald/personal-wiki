# DRF 异常处理

[How to throw custom exception with custom error code on fly in django rest framework and override default fields in exception response?](https://stackoverflow.com/questions/32764466/how-to-throw-custom-exception-with-custom-error-code-on-fly-in-django-rest-frame)

自定义 exception handler 在 `/rest_framework/views.py`，先处理 **Http404**，**PermissionDenied**，再处理 **APIException**.



https://stackoverflow.com/questions/30528088/django-rest-exceptions

