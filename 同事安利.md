# 同事安利
-----------------------------------------------------------------------------------------

```
facebook最近新出了一个PyText包，https://github.com/facebookresearch/pytext  把PyTorch限定在几个特定的NLP问题做了封装，比如：文本分类、序列标注、Joint intent-slot model、Contextual intent-slot model等。我粗略的看了文档，感觉可以用在问答的前端上。  
```

```
Joint intent-slot模型的例子：https://pytext-pytext.readthedocs-hosted.com/en/latest/atis_tutorial.html   基本上写写config.json就能训练出一个可用的深度学习模型。
```

Microservices Are Something You Grow Into, Not Begin With
https://hackernoon.com/deploying-frontend-applications-the-fun-way-bc3f69e15331
前端部署
https://hackernoon.com/the-2018-devops-roadmap-31588d8670cb
devops roadmap
https://mp.weixin.qq.com/s?__biz=MzUzNTc0NTcyMw==&mid=2247484140&idx=1&sn=20eabd51d5634e58e3b0fab1768c29ba&chksm=fa818738cdf60e2e9977b7d48ce1ff78e96bdc6d6132ab877199dab29b69a559ccc5421302dc&mpshare=1&scene=1&srcid=1106zCAtFSx7OaHqegHdHNgg&pass_ticket=iSjgBtsKaD3irSlEu1DIvv2Sma0JyMNreoVoREBHsS1bldA7AEuy%2F%2FWTixj78S%2FI#rd
文本去冲
Best way to design a database and table to keep records of changes? https://dba.stackexchange.com/questions/114580/
----------------------------------------
20181029
https://github.com/cfenollosa/os-tutorial
又看到一位造操作系统轮子的勇士，不过这个轮子的制造说明书看起来清晰明了，新老皆宜。#今日repo#
20181028
https://github.com/machinalis/quepy
quepy可以将自然语言问句转换为数据库query language，鲍老师和马博去年在quip上提到过类似的概念。#今日repo#
20181027
https://github.com/NaturalIntelligence/imglab
开源图像标注工具，支持图像分类，语义分割等操作，有兴趣的朋友可以玩玩。#今夜repo#
20181026
https://github.com/danburzo/percollate
将网页转换为PDF的命令行工具，输入URL，生成PDF。主要是调用了headless chrome. #今日repo#
20181025
https://github.com/TheAlgorithms/Python
用Python实现的算法库，包含了很多见过的没见过的算法，一起学习。#今日repo#
20181024
https://github.com/Tencent/omi
程序员节学点新东西，腾讯新出的“下一代”前端框架，前端的同学们可以试试。#今日repo#
20181023
https://github.com/pre-commit/pre-commit/
pre commit hooks检查可以帮助code reviewer节省很多时间，但是每次新建一个项目都要把hooks脚本复制来复制去很烦，尤其是某些脚本依赖了第三方包的时候。precommit 就是为解决这些问题而生的。 #今日repo# 
20181022
https://github.com/hoya012/deep_learning_object_detection
一张图带你看遍物体检测算法的演化。#今日repo#
20181021
https://github.com/astorfi/Deep-Learning-World
一个偏向researcher的深度学习资源汇总，包括最新的论文进展和数据集。#今日repo#
20101020
https://github.com/webpack-contrib/npm-install-webpack-plugin
在开发的过程中，经常会发现某个包没装。这时候不得不ctrl c终止程序，npm install之后，再重开server。这实在是不方便，怎么办？答案是：自动检测import/require语句，自动安装，自动build，浏览器端再自动刷新。#今日repo#
20181019
https://github.com/samuelcolvin/pydantic
自动根据python的type annotations进行数据格式校验，看完涨了很多关于type hints的知识。#今日repo#
20181018
https://github.com/pyeve/eve
只需要写一写json schema，eve就会自动帮助你生成CRUD API, 后端程序员可以在10分钟内setup一个服务。当然，如果想支持更多操作，eve也提供了非常多的event hook，随你定制。#今日repo#
20181017
https://github.com/mahmoud/boltons
boltons提供了一系列有用的utils，来弥补python标准库的某些功能缺失。具体有什么，点开看看就知道啦。#今日repo#
20181016
https://github.com/mmattozzi/LiteraryClockScreenSaver
适用于Mac的屏保，每一个时刻显示的时间都出自某篇小说，可以让你的桌面充满文学气息。#今日repo#
20181015
https://github.com/xonsh/xonsh
python写的shell，支持Python语法和shell脚本的混合书写。同时支持富context的history记录，我觉得能据此开发出更智能的自动补全。#今日repo#
20181014
https://github.com/pyinvoke/invoke/
类似于linux的make，ruby的rake，invoke是一个python版的自动化的任务系统。#今日repo#
20181013
https://github.com/solid/solid
周末了，学一下TBL的最新idea：去中心化的solid web。#今日repo#
20181012
https://github.com/fabric/fabric/
用于在远程主机上执行任务的Python库，可以方便的进行主机管理。如果想要批量控制主机增删改查user、开关端口，开关某些服务，可以尝试用这个库。#今日repo#
20181011
https://github.com/donnemartin/gitsome
增强版git指令，可以在命令行完成大部分需要在github网页完成的操作，适合命令行爱好者。#今日repo#
20181010
https://github.com/facebookincubator/python-nubia
Facebook新推出的命令行程序框架，支持各种自动补全。只需要各种堆叠装饰器就可以方便的自动补全，并且可以根据type annotation进行类型校验，有兴趣开发cli程序的同学可以试试。#今日repo#
20181009
https://github.com/facebookincubator/Bowler
bowler提供的API可以像操作DOM树那样操作python代码语法树，除了能学习编译原理之外，还能进行一些高级代码重构操作。#今日repo#
20181008
https://github.com/JarryShaw/f2format
将python 3.6的fstring转换成向后兼容的语法。如果将其加入到pre commit hook，就可以每个人使用自己的python 版本，转换后的代码会保证兼容性。#今日repo#
20181007
https://github.com/tobegit3hub/advisor
写过机器学习的人都知道调超参有多烦，堪称玄学，可比炼丹。advisor可以自动化的用多种算法搜索参数，让我们科学炼丹，快速炼丹，智能炼丹。#今日repo#
20181006
https://github.com/Unitech/pm2
一个类似于supervisor的进程管理工具，支持监控，重启，关机重启，docker集成，web监控等特性，感觉比supervisor更现代。#今日repo#
20181005
https://github.com/pubkey/rxdb
前端数据库的最新动态，建立在IndexedDB之上的PounchDB之上的rxdb，支持在数据库层面监听数据变化并更新UI界面，可以少写很多前后端协同代码。#今日repo#
20181004
https://github.com/zziz/pwc
pwc是paper with code的简称，这个论文集合只收集带实现代码的论文，并且按照对应repo star数排序。论文代码两相宜，哪里不会读哪里。#今日repo#
20181003
https://github.com/1tayH/noisy
随机的制造一些http请求和dns解析请求，从而让你的真实访问淹没在噪声中，适用于特别看重隐私的人。如果我们制造出足够多的garbage数据，那么大公司也训练不出什么有用的模型。#今日repo#
20181002
https://github.com/trygve-lie/circuit-b
适用于node环境的非侵入式网络断路保护器，名字是不是听起来很难懂，其实看一眼example就明白啦。基本的作用是将处理网络异常的代码和正常网络请求的业务代码解耦。#今日repo#
20181001
https://github.com/JetBrains/ring-ui
用惯了antdesign，有时候试试别的风格也不错。这个UI库是jetbrains出的，跟他家的IDE视觉风格非常一致。#今日repo#
20180930
https://github.com/0xAX/linux-insides
从代码层面理解linux，需要C语言和一点点汇编语言作为基础。#今日repo#
20180929
https://github.com/alex/what-happens-when
还记得那个经典的面试题吗：从输入网址到浏览器加载出页面，中间到底发生了什么。熟读这 repo，你能把面试官聊到怀疑人生。#今日repo#
20180928
https://github.com/getgauge/taiko
和浏览器交互的REPL环境，不只是奇技淫巧。在写前端的自动化测试时也很方便。#今日repo#
20180927
https://github.com/tldr-pages/tldr
man page虽好，但是翻起来太累。tldr让你用10s时间，可以掌握linux命令80%场景下的用法。#今日repo#
20180926
https://github.com/getgauge/gauge
这是一个神奇的库，可以根据markdown自动生成单元测试代码。支持各种类型的传递参数，非常适合数据驱动的测试，应该可以用在MemectExtractor的自动化测试上。#今日repo#
20180925
https://github.com/antonioribeiro/health
一站式服务监控平台，包括并不限于postgres, redis, supervisior等等等等，并可根据自己的需求配置其他服务。妈妈再也不担心服务挂掉了。#今日repo#
20180924
https://github.com/burtonator/polar-bookshelf
一个基于electron的知识管理软件，支持PDF和网页标注，所有的标注信息都存储为json格式。也许我们可以学习标注信息的数据结构。#今日repo#
20180923
https://github.com/lupoDharkael/flameshot
可能是Linux下最好用的截图工具，交互UI方便得不像Linux。#今日repo#
20180922
https://github.com/mvo5/apt-clone
python项目一般都用requirements.txt描述依赖的package列表，那么有没有想过生成操作系统级别的依赖包列表，方便以后重装系统，apt-clone可以帮你完成这件事。#今日repo#
20180921
https://github.com/sharkdp/bat (https://github.com/sharkdp/bat?from=singlemessage&isappinstalled=0)
高级版cat命令，支持语法高亮，分页，git集成，推荐给经常要部署的同学们。#今日repo#
2018-11-02
文本分类是我们在项目中经常遇到的问题：问答的意图分类、黑嘴项目中的黑嘴判断等等。https://realpython.com/python-keras-text-classification/  这是一篇非常完善的文本分类教程，从最普通的LR，到NN，再到CNN都有介绍，大家有兴趣可以读读
王爽: https://github.com/ChrisKnott/Algojammer  这个好像是教人学算法的IDE，看起来还挺好玩的。
Google前段时间发布了一个据说很厉害的语言表示模型BERT，这两天终于release了对应的代码和pretrained model，模型有中文的哦。#今日repo#
https://github.com/google-research/bert
https://github.com/nbedos/termtosvg
terminal 录屏幕