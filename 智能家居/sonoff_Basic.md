# Sonoff basic

[**espurna-1.13.3-itead-sonoff-basic.bin 固件**](https://github.com/xoseperez/espurna/releases/download/1.13.3/espurna-1.13.3-itead-sonoff-basic.bin)

[Sonoff Basic (3V3, Rx, Tx, GND)  位置](https://github.com/arendst/Sonoff-Tasmota/wiki/Sonoff-Basic#gpio-locations)

插入 rx, tx, GND（注意 rx tx 连接方式），按住黑色按钮，同时插入 3.3v（vccio）**黄** 线（PL2303编程器带的线），进入flash mode。

![sonoff_basic](http://wiki-picture.oss-cn-beijing.aliyuncs.com/sonoff_basic.png)

