# Sonoff Itead 1CH Inching 刷固件

[Itead 1CH Inching espurna](https://github.com/xoseperez/espurna/wiki/Hardware-Itead-1CH)

[espurna-1.13.3-itead-1ch-inching.bin 固件下载](https://github.com/xoseperez/espurna/releases/download/1.13.3/espurna-1.13.3-itead-1ch-inching.bin)

[淘宝：1CH Inching](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.554a2e8dpZj4Tj&id=530787872729&_u=g5034ps43a1)

Itead 1CH Inching 是一个单路的继电器，可以用于无源信号的通断，例如锅炉地暖或者暖气的启停，新风的启停等控制。具体例子见：

TODO 地暖的例子 新风的例子

## Flash Mode

在卖的可能分为两种型号 [PSF-B01](https://github.com/xoseperez/espurna/wiki/Hardware-Itead-1CH#flashing-psf-b01) 和 [PSA-B01](https://github.com/xoseperez/espurna/wiki/Hardware-Itead-1CH#flashing-psa-b01)，它们对应的 Flashing 方法不同，我在淘宝买过几个，型号均为 PSF-B01。

![sonoff_itead_1ch_Inching](http://wiki-picture.oss-cn-beijing.aliyuncs.com/1ch-inching-psfb01-flash.jpg)

![sonoff_psf_b01_2](http://wiki-picture.oss-cn-beijing.aliyuncs.com/1ch-inching-psfb01-flash2.jpg)

1. 根据图1的标注，如图2，将GND（棕色）连好。
2. 黑色线 5v input 那里同芯片连好，rx，tx连好（rx，tx互换）。
3. 3.3v （红色）连好，同时松开黑色线。

## 刷固件

[espurna-1.13.3-itead-1ch-inching.bin](https://github.com/xoseperez/espurna/releases/download/1.13.3/espurna-1.13.3-itead-1ch-inching.bin)

进入 Flash mode 后,

```bash
esptool.py -b 115200 --port /dev/ttyUSB0 write_flash --flash_freq 80m --flash_mode dout 0x000000 espurna-1.13.3-itead-1ch-inching.bin
```

100%显示完后，打开 wifi，寻找Espruma开头的wifi，账号为 admin 密码为 fibonacci。

