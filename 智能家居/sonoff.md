# Sonoff

[Sonoff 淘宝店](https://itead.taobao.com/?spm=2013.1.1000126.3.7f69b979I6RcyC)

[(中文固件发布) 极其适合新手的 Sonoff 固件 - ESPurna](https://bbs.hassbian.com/forum.php?mod=viewthread&tid=2550&highlight=%E5%9B%BA%E4%BB%B6) 

[sonoff 进入 flash mode | 连线](https://github.com/arendst/Sonoff-Tasmota/wiki/Hardware-Preparation)

[Sonoff RF Bridge 设置刷固件 ESPurna](https://bbs.hassbian.com/forum.php?mod=viewthread&tid=1827&highlight=ESPurna)

## sonoff 固件

可以通过两种方式刷固件，一种不用编程器，直接通过wifi连接，例如 SonOTA，缺点为新版本的 sonoff 可能失败。另一种方式为通过 [编程器PL2303](https://detail.tmall.com/item.htm?id=543221675162&spm=a1z09.2.0.0.52732e8dUVdPI1&_u=g5034ps37c2) 连接sonoff，再利用 esptool，[arduino](https://github.com/xoseperez/espurna/wiki/ArduinoIDE)，[PlatformIO](https://github.com/xoseperez/espurna/wiki/PlatformIO)等工具传输。不同的 sonoff 设备进入 flash mode 的方式不一样，详见 [具体型号列表](https://github.com/xoseperez/espurna/wiki/Hardware#itead-sonoff-modules)。

| 型号              | 描述                 |
| ----------------- | -------------------- |
| Sonoff Basic      | 开关（220V 通断）    |
| ITEAD 1CH Inching | 开关 无源 继电器     |
| Sonoff 4CH PRO    | 开关 无源 继电器 4路 |

### sonoff 通过 wifi 刷固件

[SonOTA](https://github.com/mirko/SonOTA) 可以将 [Sonoff-Tasmota](https://github.com/arendst/Sonoff-Tasmota) | [espurna](https://github.com/xoseperez/espurna/wiki/OTA-flashing-of-virgin-Itead-Sonoff-devices) 固件通过wifi 直接上传到 sonoff 实体上。

```bash
git clone https://github.com/mirko/SonOTA.git
virtualenv --python=python3.6 py36env
./py36env/bin/pip3 install -r requirements.txt

./py36env/bin/python sonota.py
# ..... 剩下按照步骤 等待 FinalStep ssid 出现
```

### sonoff 通过 PL2303 编程器刷固件

[Esptool](https://github.com/arendst/Sonoff-Tasmota/wiki/Esptool) 可以通过命令行直接刷固件，**连线方式为**：

| Programmer（PL2303） | Sonoff Module |
| -------------------- | ------------- |
| 3V3(vccio)           | 3V3 / VCC     |
| **TX**               | **RX**        |
| **RX**               | **TX**        |
| GND                  | GND           |

编程器的 TX 接 sonoff 的 RX，RX 接 sonoff 的 TX。

**进入 Flash Mode**：

1. GPIO0 和 GND 连通（一般通过按板子上的黑钮）按住！！！
2. 按上面的表格连线，将 sonoff 和 编程器连起来。
3. 1-2秒后，断开 GPIO0 和 GND （松开黑纽）。

在文件夹中用 pipenv 开启虚拟环境，安装 esptool，进入虚拟环境。

```bash
# 命令行 Ubuntu
pipenv --python 3.6
pipenv install esptool
pipenv shell
```

```bash
esptool.py -b 115200 --port /dev/ttyUSB0 write_flash --flash_freq 80m --flash_mode dout 0x000000 espurna-1.13.3-itead-sonoff-basic.bin 
```

- `/dev/ttyUSB0` 为编程器的连接位置？ 如果不行试一试 `/dev/ttyUSB1`

- espurna-1.13.3-itead-sonoff-basic.bin 为固件文件名称，可以在 [release](https://github.com/xoseperez/espurna/releases) 中下载。

**MQTT 配置**：

[espurna MQTT 设置](https://github.com/xoseperez/espurna/wiki/MQTT)

在刷好固件后，重新给 sonoff 板供电，这时出现的 wifi 例如 `ESPURNA-2D3BAC`，初始密码为 `fibonacci`，登录 `192.168.4.1`。**如果刷机后没有出现热点，尝试长按按钮10秒恢复设置**。选择左侧的 **wifi** 选项，添加连接路由器wifi的账号密码。点击 **MQTT**，**MQTT server** 填写对应的 mqtt server 安装位置的 ip：`192.168.1.61`. **Port** 为 `1883`，账号密码均为 `mqtt`。

TODO 关联 如何 搭建MQTT server 

**NTP 配置**

NTP 中 **Enable DST** 选 **No**，时区选 **GMT+8**。
