# Home Assistant



[HA 中文网](https://www.hachina.io/)

[Homeassistant  群晖 docker 方法安装 | 更新](https://www.home-assistant.io/docs/installation/docker/#synology-nas)

1. 在 Synology NAS 安装 docker
2. 在 “Registry” 中搜索 `homeassistant/home-assistant` ，更新时只会覆盖当前 image。
3. 其他见文档。

Ubuntu 安装

```bash
pipenv --python 3.6
mkdir homeassistant / cd homeassistant /
pipenv install homeassistant
```



Mqtt switch



[mqtt switch 多个时的配置写法](https://community.home-assistant.io/t/howto-add-multiple-sonoff-touch-switches-flashed-with-tasmota/44765/2?u=zbjdonald)

```bash
2018-12-01 17:10:13 DEBUG (MainThread) [homeassistant.components.mqtt] Received message on homeassistant/switch/ESPURNA-8570E5_0/config: {"name":"ESPURNA_8570E5_0","platform":"mqtt","state_topic":"S3_pump/relay/0","command_topic":"S3_pump/relay/0/set","payload_on":"1","payload_off":"0","availability_topic":"S3_pump/status","payload_available":"1","payload_not_available":"0"}
2018-12-01 17:10:13 INFO (MainThread) [homeassistant.components.mqtt.discovery] Found new component: switch ESPURNA-8570E5_0
```



