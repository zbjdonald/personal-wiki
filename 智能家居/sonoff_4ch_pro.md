# Sonoff 4CH Pro 刷固件

[**espurna-1.13.3-itead-sonoff-4ch-pro.bin 固件**](https://github.com/xoseperez/espurna/releases/download/1.13.3/espurna-1.13.3-itead-sonoff-4ch-pro.bin)

[Sonoff 4CH Pro 固件教程](https://diyprojects.io/hack-sonoff-4ch-pro-firmware-mqtt-tasmota-inclusion-domoticz/#.W9HKZ3UzYUE)

连接左侧三线，RX，TX，GND 后

![sonoff_4ch_pro_1](http://wiki-picture.oss-cn-beijing.aliyuncs.com/sonoff_4ch_pro_1.png)

按照此图连线，然后接通 5-24v 电源，进入 flash mode。

![sonoff_4ch_pro_2](http://wiki-picture.oss-cn-beijing.aliyuncs.com/sonoff_4ch_pro_2.png)

```
esptool.py --port /dev/ttyUSB0 erase_flash # 删除原始固件
```

删除固件后需要重新重复 flash 这个过程，再次进入 flash 模式后刷入固件。