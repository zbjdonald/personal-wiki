# PL2303 编程器

[淘宝：排针 | 排母](https://clfsm.tmall.com/category-1246924780.htm?spm=a220o.1000855.w4010-21296030266.39.5acc3139l9xSMF&search=y&parentCatId=967622614&parentCatName=%BD%D3%B2%E5%BC%FE&catName=%C5%C5%D5%EB%2F%C5%C5%C4%B8#bd)

[淘宝：杜邦线](https://detail.tmall.com/item.htm?spm=a1z10.5-b.w4011-21296030279.63.2de66214aYY6zB&id=537487524581&rn=067fc2740fdadd92e5efd70159bd192b&abbucket=15&skuId=3208169891312)

[淘宝：PL2303 编程器](https://detail.tmall.com/item.htm?spm=a1z10.5-b.w4011-21296030279.94.2d586214iukNHH&id=543206372721&rn=92dfa856e51bbc29c808fe38def96e51&abbucket=15)

[PL2303 windows 驱动](http://www.waveshare.net/wiki/PL2303_USB_UART_Board_(mini))



PL2303 编程器可用于 sonoff 刷第三方固件，一头连 sonoff 板上焊接的排针，一头连电脑 usb，连电脑 usb 的一端需要注意一下，不同型号的连接线不一样：[PL2303 usb 直连](https://detail.tmall.com/item.htm?spm=a1z10.5-b.w4011-21296030279.94.2d586214iukNHH&id=543206372721&rn=92dfa856e51bbc29c808fe38def96e51&abbucket=15)，[mini usb 口（上古口）](https://detail.tmall.com/item.htm?spm=a1z10.3-b.w4011-21296023211.20.e5c2f3a32vBoYk&id=543221675162&rn=cb585d81ddf01ad16cb5cecf955c0405&abbucket=15)，[micro usb 口 （非type-C 那种安卓口）](https://detail.tmall.com/item.htm?spm=a1z10.3-b.w4011-21296023211.23.e5c2f3a32vBoYk&id=543248455620&rn=cb585d81ddf01ad16cb5cecf955c0405&abbucket=15)。连 sonoff 排针| 排母那一头连杜邦线，[杜邦线](https://clfsm.tmall.com/category-1246924780.htm?spm=a220o.1000855.w4010-21296030266.39.5acc3139l9xSMF&search=y&parentCatId=967622614&parentCatName=%BD%D3%B2%E5%BC%FE&catName=%C5%C5%D5%EB%2F%C5%C5%C4%B8#bd)分公母，买的时候注意一下。



Windows 下安装驱动见 [PL2303 USB UART Board (mini) 文档](http://www.waveshare.net/wiki/PL2303_USB_UART_Board_(mini))。

Ubuntu 下查看连接是否正常：

```bash
# Ubuntu 查看是否连入
modprobe usbserial vendor=0x067b product=0x2303 
dmesg | grep 'ttyUSB'
sudo chmod 777 /dev/ttyUSB0 
cu -l /dev/ttyUSB0 -s 9600
```

