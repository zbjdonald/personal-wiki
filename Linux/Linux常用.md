# Linux 常用

https://opensource.com/education/16/10/simplescreenrecorder-and-kazam

截图工具





新服务器到手后快速实现配置，例如 SSH，防火墙，创建用户等操作。[Bash setup script for Ubuntu servers](https://github.com/jasonheecs/ubuntu-server-setup) 脚本可以快速配置。

```bash
sudo apt-get update
sudo apt-get install git
cd
git clone https://github.com/jasonheecs/ubuntu-server-setup.git
cd ubuntu-server-setup
bash setup.sh
```

> 时区换成 [Asia/Shanghai](https://en.wikipedia.org/w/index.php?title=Asia/Shanghai&action=edit&redlink=1)
>
> copy 客户端 ssh public key `xclip -sel clip < ~/.ssh/id_rsa.pub`

- 防火墙设置 | [Set Up a Firewall with UFW on Ubuntu 16.04 or others](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-16-04)

- 创建 sudo 用户 | [Create a Sudo User on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart)

- 服务器初始化设置 | [Initial Server Setup with Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)





安装

[How do I install a .deb file via the command line?](https://askubuntu.com/questions/40779/how-do-i-install-a-deb-file-via-the-command-line)



https://superuser.com/questions/490138/why-do-i-have-so-many-ipv6-inet6-addresses/490149

https://askubuntu.com/questions/141397/why-do-i-get-multiple-global-ipv6-addresses-listed-in-ifconfig/141526

为什么有多个 ipv6

# Getting Started with UFW

https://www.howtoforge.com/tutorial/ufw-uncomplicated-firewall-on-ubuntu-15-04/

https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands

https://askubuntu.com/a/1019237

ufw delete

https://askubuntu.com/questions/593886/ufw-wont-delete-rule

https://askubuntu.com/questions/652556/uncomplicated-firewall-ufw-is-not-blocking-anything-when-using-docker



docke -p 的问题

docker 和 ufw 的关系

ufw docker?



开启一个简单的 http 服务器，测试连接是否通畅

```bash
python -m SimpleHTTPServer 7899
```

获取当前 host ip 信息

```bash
curl ip.gs
Current IP / 当前 IP: 125.34.212.140
ISP / 运营商:  ChinaUnicom
City / 城市: Beijing Beijing
Country / 国家: China
  /\_/\
=( °w° )=
  )   (  //
 (__ __)//
```

开放 ufw 7899 端口用于测试上一步的ip `http://125.34.212.140:7899`，测试好后删除

```
sudo ufw allow 7899
sudo ufw delete allow 7899
```

netstat -lntup



ncdu 用于发现文件夹中的使用状况 可以用于释放空间