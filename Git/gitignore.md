# .gitignore

[.gitignore file - ignoring files in Git | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/saving-changes/gitignore)

[.gitignore is not working](https://stackoverflow.com/questions/11451535/gitignore-is-not-working)

[如何写 .gitignore 文件 | ignore patterns](https://www.atlassian.com/git/tutorials/saving-changes/gitignore#git-ignore-patterns)

Git 将文件分为 3 种状态：

1. tracked -  staged or committed;
2. untracked - not staged or committed; 
3. ignored - Git explicitly ignored.

Ignored 文件通常包括 dependency caches, compiled code, hidden system files, 和 personal IDE config files等。Git ignore 有三种控制方式，在 repository 中与他人中共享 ignore rules，在 repository 中的设置私有的 ignore rules， 以及私有的全局 ignore rules。

Debug ignore 文件时，可以用这个查看 ignore 文件

```bash
git ls-files --others -i --exclude-standard
```



### Repository 的共用 ignore rules

通常将 Repo 相关人员共用的 `.gitignore` 文件 Commit 进 Repo 的根目录，例如 `.gitignore` 可能包含的环境变量配置的文件等，可以参考 [sample file](https://github.com/wemake-services/wemake-django-template/blob/master/%7B%7Bcookiecutter.project_name%7D%7D/.gitignore)。`.gitignore` 也可以放在多个文件夹中，对应的 ignore pattern 也相对那个文件夹。

### Repository 的私有 ignore rules

在项目的根目录中打开 `.git/info/exclude`，写入私有的 ignore patterns. 这些 pattern 不会被 versioned，所以可以写入一些个人配置。例如在项目根目录中有一个文件夹 `scripts` 存放一些脚本，这些脚本不想跟着 branch 走，可以在 `.git/info/exclude` 中写入

```
/scripts/
```

 当 checkout 到其他 branch 时，`/scripts/` 文件夹不会消失。

### 全局 ignore rules

你可以对 local system 所有 repository 设置 ignore rules，但是需要设置 git global config

```
vim ~/.gitignore
git config --global core.excludesFile ~/.gitignore
```

> home 目录中创建一个隐藏的 gitignore, git 全局配置指定这个文件。

例子：在全局设置中可以忽略 pycharm 的配置文件，因为同组的开发人员可能不全都是使用 pycharm。在 ignore 文件中添加 `.idea/`

----

### Ignore committed file

如果文件已经 commit 过，在 `.gitignore` 中添加忽略该文件的 pattern 没有用。需要先从 repository 中删除该文件，但不从 working directory 从删除。

```bash
echo debug.log >> .gitignore
git rm --cached debug.log
git commit -m "Start ignoring debug.log"

# 删除文件夹
git rm -r --cached .idea/
```

> 先在 `.gitignore` 中填入 patterns，然后用 `git rm --cached`从 repo 从删除，但在 working directory 中保留，然后 commit。不加 `--cached` 表示从 repo 和 working directory 中都删除。