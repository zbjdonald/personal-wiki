# Caddy

[Caddy 中文文档](https://dengxiaolong.com/caddy/zh/)

[使用 Caddy 替代 Nginx，全站升级 https，配置更加简单](https://my.oschina.net/diamondfsd/blog/897301)

[Caddy Reverse Proxy Tutorial](https://medium.com/bumps-from-a-little-front-end-programmer/caddy-reverse-proxy-tutorial-faa2ce22a9c6)

[How To Host a Website with Caddy on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-host-a-website-with-caddy-on-ubuntu-16-04)

Caddy 是一个go写的web服务器，可以用来做反向代理 (Reverse Proxy) 等等，可以替代 Nginx 优点是配置简单清晰，方便配置 https。Caddy 的 prebuilt binaries 由于 licensing 的问题不能商用，比较方便的做法是用 docker 镜像，例如 [caddy-docker](https://github.com/wemake-services/caddy-docker)

```bash
docker run -p 2015:2015 -v CaddyFilePath:/etc/Caddyfile wemakeservices/caddy-docker
```

或者用于 docker-compose file，详见 [docker-compose caddy example](https://github.com/wemake-services/caddy-docker#docker-compose-example)。

### Caddy debug



### Caddy proxy 反向代理

[What is a Reverse Proxy vs. Load Balancer?](https://www.nginx.com/resources/glossary/reverse-proxy-vs-load-balancer/)

[Caddy proxy指令详解](https://toaco.github.io/2018/03/05/Caddy%20proxy%E6%8C%87%E4%BB%A4%E8%AF%A6%E8%A7%A3/)

A reverse proxy accepts a request from a client, forwards it to a server that can fulfill it, and returns the server’s response to the client.

反向代理为将客户端的请求转给服务器，再把服务器的结果转给客户端。

### Caddy rewrite

[`root` for static files, `proxy` for the rest (same root)](https://github.com/mholt/caddy/issues/695)

[The components of a URL](https://www.ibm.com/support/knowledgecenter/en/SSGMCP_5.1.0/com.ibm.cics.ts.internet.doc/topics/dfhtl_uricomp.html)

[The Anatomy of a URL: Protocol, Hostname, Path, and Parameters all in one string of content](https://www.gilliganondata.com/index.php/2012/05/22/the-anatomy-of-a-url-protocol-hostname-path-and-parameters/)

部署 SPA 应用时，需要重定向到 index.html，这时需要用到 rewrite。先看一下 Caddy 的占位符[(Placeholder)](https://caddyserver.com/docs/placeholders). 一个 URL 的结构为：

```bash
scheme://host:port/path?query
```

Caddy 占位符中的 `{path}` 表示 URL 结构中的 **path** 不包括后面的 **? #**, `{query}`表示 URL 结构中的 query，不包括前面的 **?**.























caddy file

https://my.oschina.net/diamondfsd/blog/897301

https://www.zybuluo.com/zwh8800/note/844776#2-%E9%85%8D%E7%BD%AE

caddy 教程

wemake service caddy 讨论

https://github.com/wemake-services/wemake-django-template/issues/379

https://github.com/wemake-services/wemake-django-template/issues/359

https://github.com/wemake-services/wemake-django-template/issues/359

https://medium.com/@rothgar/how-to-debug-a-running-docker-container-from-a-separate-container-983f11740dc6





