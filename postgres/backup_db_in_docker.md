# Use docker backup postgres locally

[github: docker-postgres-backup-local](https://github.com/prodrigestivill/docker-postgres-backup-local)

[postgres-backup-local docker-compose example](https://github.com/vchaptsev/cookiecutter-django-vue/blob/49fd1923d06086157260aa47f8fdd826ab4c2584/%7B%7Bcookiecutter.project_slug%7D%7D/docker-compose-prod.yml#L41)

```
services:
  db:
    image: "hub.wenyinhulian.cn/qiaochang/postgres:9.6.9"
    restart: unless-stopped
    volumes:
      - pgdata:/var/lib/postgresql/data
    networks:
      - webnet
    env_file: ./config/.env
    
  backups:
    image: "hub.wenyinhulian.cn/qiaochang/postgres-backup-local:9.6"
    restart: on-failure
    depends_on:
    - db
    environment:
      POSTGRES_HOST: db
    volumes:
    - qiaochang_db_backup:/backups/
    networks:
    - webnet
    env_file: ./config/.env
```

/var/lib/docker/volumes/qiaochang_prod_qiaochang_db_backup/_data/

 