# psql 使用方法

[17 Practical psql Commands](http://www.postgresqltutorial.com/psql-commands/)

[psql 文档](https://www.postgresql.org/docs/10/app-psql.html)

[How do I specify a password to psql non-interactively?](https://stackoverflow.com/questions/6405127/how-do-i-specify-a-password-to-psql-non-interactively)

[postgres-cheatsheet](https://gist.github.com/Kartones/dd3ff5ec5ea238d4c546)



先如何连上数据库？

```bash
PGPASSWORD='password'
psql -h 'server_name' -U 'user_name' -d 'db_name' \
     -c 'command' (eg. "select * from schema.table")
```

```bash
psql -U username -d databasename -f drop_connections.sql
```

连入对应用户数据库

```bash
psql -h 'serve_rname' -U 'user_name' -d 'db_name' -W # 交互输入密码
```

切换数据库（没有 username，默认为当前username），当前连接关闭。

```
\c dbname username
```

显示用户

```bash
\du
```

显示数据库

```bash
\l
```

显示table

```bash
\dt
```

显示table详情

```
\d table_name
```



[psql: FATAL: Peer authentication failed for user “postgres” (or any user)](https://gist.github.com/AtulKsol/4470d377b448e56468baef85af7fd614)



