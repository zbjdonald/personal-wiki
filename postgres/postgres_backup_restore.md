# PostgreSQL Backup and restore Databases

[Backing Up Databases Using PostgreSQL Backup Tools](http://www.postgresqltutorial.com/postgresql-backup-database/)

[pg_dump 文档](https://www.postgresql.org/docs/10/app-pgdump.html) [pg_dump中文文档](http://www.postgres.cn/docs/10/app-pgdump.html)

[pg_dump examples](https://www.postgresql.org/docs/9.3/app-pgdump.html#PG-DUMP-EXAMPLES) 

## 备份数据库

@(pg_dump | pgdump | 数据库导出 | 导出数据库 | 导出数据)

备份前需要考虑的问题：


- Full / partial databases
- Both data and structures, or only structures, or only data
- Point In Time recovery
- Restore performance

导出在`..23`服务器上端口号为`5429`用户名为`username`的数据库`db_name`的表`table_name`，**包含数据和schema结构**，导出格式为 tar，导出名称为 `xxxx.tar`.

```bash
pg_dump -U username -h 192.168.0.23 -p 5429 -d db_name -t table_name -Ft -a> xxxx.tar
```

- -U 表示连接用的用户名
- -h 表示 pg_server 所在的主机，如果不指定 -h, a Unix domain socket connection is attempted
- -p Defaults to the `PGPORT` environment variable，或者为 compiled-in default，5432
- -d 表示数据库名称，-t 表示表名
- -a (--data-only) 表示**只 dump 数据**，不 dump schema 定义。
- -F 表示指定输出文件的格式 （[文档](https://www.postgresql.org/docs/10/app-pgdump.html#Examples)中搜索 `--format=format`）

不同的文件输出格式有不同的用法和适用条件，-F 指定的格式有四种：

  - **p** 表示 sql script（默认）
  - **c** 表示 custom-format archive，可用于 pg_restore，已压缩。
  - **d** 表示 directory-format archive，可用于 pg_restore，每一个文件表示一个 table。
  - **t** 表示 tar-format archive，打包了 directory-format 可用于 pg_restore，不支持压缩。

pg_resotre 仅支持Fc/Ft/Fd格式的导出文件，Fp格式的文件是sql脚本，需要使用psql工具导入脚本。**建议选用 Fc 格式导出文件**。**Fc** 格式可以有选择的 restore only some tables/schema，可以选择是否包含 only schema, only data, or both 。**Fc** 格式相关时间参考：

| command         | dump time  | file size | restore time |
| --------------- | ---------- | --------- | ------------ |
| pg_dump         | 4m22.654s  | 13G       | 34m56.354s   |
| pg_dump -Fc     | 11m12.443s | 4.6G      | 34m27.218s   |
| pg_dump -Z0 -Fc | 4m18.742s  | 14G       | 36m8.156s    |
| pg_dump -Z6 -Fc | 11m18.154s | 4.6G      | 35m54.344s   |

### 例子

```
# 将本地 qiaochang 数据库以 custom 方式导出
pg_dump -U admin -h localhost -d qiaochang1 -Fc > qiaochang.dump
```

### Further reading:

[why use custom-format?](https://dba.stackexchange.com/a/76431)

[Use costom format to efficiently backup and restore table](http://zevross.com/blog/2014/06/11/use-postgresqls-custom-format-to-efficiently-backup-and-restore-tables/)

[pg_dump format: custom vs sql script](https://dba.stackexchange.com/questions/49045/pg-dump-format-custom-vs-sql)

[Using compression with PostgreSQL’s pg_dump](https://dan.langille.org/2013/06/10/using-compression-with-postgresqls-pg_dump/)

[Does my choice of pg_dump format impact restore speed?](https://dba.stackexchange.com/questions/8855/does-my-choice-of-pg-dump-format-impact-restore-speed)

### 问题：

**导出database时是否只需要dump public ?**

> One final note for those who use PostGIS. By default, creating a dump file dumps all the schema including the `tiger`, `tiger_data` and `topology` schema and I've found restoring these results in a lot of error messages. To dump only the `public` schema use:
>
> pg_dump -U postgres -n public -Fc myDB > myDB.dump

## 删除数据库

[dropdb文档](https://www.postgresql.org/docs/10/app-dropdb.html)

@(关闭数据库 | drop database | drop db | 关闭连接 | 删除链接 | 删除连接)

当数据库存在 connections 的时候不能直接删除，需要先 [drop connections](https://stackoverflow.com/a/5408501)，先把脚本 drop_connections.sql 放入psql接触到的地方（更改需要 drop ）,

```sql
-- drop_connections.sql
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'TARGET_DB' --change this to your DB
      AND pid <> pg_backend_pid();
```

然后执行

```bash
psql -U username -d databasename -f drop_connections.sql
```

`-d` 所指定的数据库不能为准备删除的数据库('TARGET_DB')，一般 `-d` 可以用 `postgres`

关闭连接后，删除数据库，username 需要有权限删除 target database

```bash
dropdb -U username databasename
```

## 恢复数据库

@(pg_restore | 数据库导入 | 导入数据库)

[pg_restore使用](https://my.oschina.net/yafeishi/blog/742307)

[PostgreSQL Restore Database](http://www.postgresqltutorial.com/postgresql-restore-database/)

[pg_restore 文档](https://www.postgresql.org/docs/10/app-pgrestore.html) [pg_restore 中文文档](http://www.postgres.cn/docs/10/app-pgrestore.html)

[pg_restore examples](https://www.postgresql.org/docs/10/app-pgrestore.html#APP-PGRESTORE-EXAMPLES)

```bash
pg_restore -U username -a dump_file_name
```

- `-a` (--data-only)  表示只恢复数据，不恢复 schema。

```bash
pg_restore -C -c postgres db.dump
```

- `-C`(create) Create the database before restoring into it.
- `-c`(clean) drop database objects before recreating them.

同时指定 `-c`, `-C` drop and recreate the target database before connecting to it. options `-c/`--clean and `-a/`--data-only cannot be used together. `-c` `-a` 不能同用。

如果需要类似于 `psql` 的 --quiet 模式可以尝试

```bash
pg_restore --cluster 8.4/mycluster mycluster.dump > pg_restore.log
```

[pg_restore 部分原理](https://stackoverflow.com/a/11493138)

> `pg_restore` has two modes of operation, with or without connecting to a database. When it's called without a database (`-d` option) as shown in the question.then its sole purpose is to output a set of SQL commands in plain text that should be fed to an SQL interpreter to restore the database. Those SQL commands form a coherent set without any concept of verbosity, and they are **not executed** by `pg_restore` itself. They're generally redirected into a file for later execution or piped into `psql` for immediate execution.

### 例子

关闭活动数据库连接，将备份的数据库恢复，更改恢复数据库的名字 @(更改数据库名 | 修改数据库名)

```bash
psql -U username -d databasename -f drop_connections.sql
dropdb -U username dbname
pg_restore -U username YOUR_DUMP_FILE

psql -U username -c "ALTER DATABASE db RENAME TO newdb;"
```

### 问题：

在删除数据库后，恢复数据库时 `pg_restore  -C` 是否会先创建数据库，然后将旧库里的数据导入新库名，从而不用改名？

> 待试验

数据库内有数据时，pg_restore -a 是否直接覆盖数据，还是增量添加 ?

> pg_restore -c 时，会完全替换成 dump 好的数据，详见[实验](https://imgur.com/a/Mzj0c)。

每次 pg_restore 前是否需要 drop 数据库？

[pg_restore --clean is not dropping and clearing the database](https://stackoverflow.com/questions/42481346/pg-restore-clean-is-not-dropping-and-clearing-the-database)

这里在质疑 pg_restore --clean 恢复不完全？ 

相关讨论：

> *Sometimes `-c` --clean can't drop tables that are in use, and it seems to just plow-on-ahead anyway. This can cause lots of import errors (duplicate keys) and rows still existing that you don't expect.*

## 备份恢复常见问题

> IntegrityError: null value in column “id” for all models/fields with ForeignKey after postgres restore from dump

当从高版本 10.5 dump 后，在低版本 9.6, 9.5 restore 时，会出现这个问题。[解决方法](https://stackoverflow.com/a/47824894)：

1. 导出 raw sql 的版本，不用 custom-format 模式，清空数据库后重新用 psql 导入。
2. 用相同数据库版本重新导入。