# Git 教程

[Sample of git config file (Example .gitconfig)](https://gist.github.com/pksunkara/988716#file-gitconfig)



#### [replace branch | 替换分支](https://stackoverflow.com/a/2862606)

@(Overwrite master | replace master | overwrite branch | replace branch)  

例如将 master 替换成 XXXXX 分支， 在 XXXXX 分支中（**此种方法会丢失 master 历史 commit!!!!**），替换 master，developer 等受保护分支时，先去[关闭保护](https://docs.gitlab.com/ee/user/project/protected_branches.html#configuring-protected-branches)。


```bash
git branch -m master old-master
git branch -m XXXXX master
git push -f origin master
```

> 在 XXXXXX 分支中，先将 master rename 成 old-master，再将 XXXXX rename 成 master，最后将 remote master 替换成本地 master。

related: [git push --forceand how to deal with it](https://evilmartians.com/chronicles/git-push---force-and-how-to-deal-with-it)

#### [checkout remote branch | checkout 远程分支](https://stackoverflow.com/a/1783426)

```bash
git checkout -b test <name of remote>/test
```

